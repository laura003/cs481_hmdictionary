﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Net.Http;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Dictionary
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    // Class generate by https://app.quicktype.io/#l=cs&r=json2csharp
    public partial class Definitions
    {
        [JsonProperty("definitions")]
        public Definition[] DefinitionsDefinitions { get; set; }

        [JsonProperty("word")]
        public string Word { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
    }

    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public object Emoji { get; set; }
    }
    public partial class MainPage : ContentPage
    {
        // variable for web request
        HttpClient client;
        //api url
        private static string api = "https://owlbot.info/api/v4/dictionary/";
        //token generate by https://owlbot.info with my mail address
        private static string token = "994df361130761ff052f51daccaadb9cb3c36bae";


        public MainPage()
        {
            client = new HttpClient();
            //Send Token authorisation
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", token);
            InitializeComponent();
        }

        public async void WordDefinition(object sender, EventArgs args)
        {
            // Verify internet connction or not
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {
                // get request to api
                var success = await client.GetAsync(api + this.search.Text);
                // Verify success request or not
                if (success.IsSuccessStatusCode)
                {
                    // get content response to json and deserialize json response
                    Definitions response = JsonConvert.DeserializeObject<Definitions>(success.Content.ReadAsStringAsync().Result);
                    // put word search on the screen
                    this.word.Text = response.Word;
                    // verify if array isn't null
                    if (response.DefinitionsDefinitions.Length > 0)
                    {
                        // put definition search on the screen
                        this.definition.Text = response.DefinitionsDefinitions[0].DefinitionDefinition;
                        // put example search on the screen
                        this.example.Text = response.DefinitionsDefinitions[0].Example;
                        // put type search on the screen
                        this.type.Text = response.DefinitionsDefinitions[0].Type;
                    }
                }

                else
                {
                    // display error message if request fail
                    await DisplayAlert("Error", success.ReasonPhrase, "OK");
                }
            }
            else
            {
                //display error message if no internet
                await DisplayAlert("Error", "No internet, try again.", "OK");
            }
        }

    }
}
